#!/usr/bin/env python3

# add redis

import telepot
import yaml
import time
import os
from raven.handlers.logging import SentryHandler
from bitshares.asset import Asset

os.chdir(os.path.dirname(os.path.abspath(__file__)))
with open('settings.yaml') as f:
    settings = yaml.safe_load(f.read())

chat_id = settings['chat_id']
bot = telepot.Bot(settings['tg_token'])

notify_msg = """*{asset}:BTS*
Debt: {debt} {asset}
Collateral: {collateral} BTS

"""


def process_asset(asset_name):
    asset = Asset(asset_name)
    call_orders = asset.get_call_orders()
    call_orders = [c for c in call_orders if c['ratio'] < 1.75]
    debt_sum, collateral_sum = 0, 0
    for call_order in call_orders:
        debt_sum += call_order['debt'].amount
        collateral_sum += call_order['collateral'].amount
    msg = notify_msg.format(
        asset=asset_name,
        debt=round(debt_sum, 3),
        collateral=round(collateral_sum, 3))
    return msg


if __name__ == '__main__':
    assets = ['USD', 'CNY', 'RUBLE', 'BTC', 'HERO']
    while True:
        msg = ''.join([process_asset(asset) for asset in assets])
        bot.sendMessage(chat_id, msg, parse_mode="Markdown")
        time.sleep(settings['check_interval'])
